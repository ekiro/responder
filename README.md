# Responder

## Test project

### Console

Install libraries

`pip install -r requirements.py`

Install project

`python setup.py install`

Run project

`python run.py`

### Online

Visit https://piotr.karkut.info/responder

## Configuration

You can edit standard responses in data/conversation.yaml
