import asyncio

import websockets

from responder.conversation.conversation import Conversation
from responder.conversation.model import Config, User


class WebServer:
    def __init__(self, config: Config) -> None:
        self._config = config

    async def handle_ws(self, websocket, path) -> None:
        name = await websocket.recv()
        user = User(name=name)
        conv = Conversation(user=user, config=self._config)
        while True:
            text = await websocket.recv()
            if text:
                await websocket.send(conv.message(text))

    def run(self) -> None:
        start_server = websockets.serve(self.handle_ws, "localhost", 8112)

        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()
