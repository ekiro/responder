import re
from dataclasses import asdict
from typing import Dict, Optional

from responder.conversation.model import User, Config
from responder.conversation.plugins import Plugins


class Conversation:
    def __init__(self, user: User, config: Config) -> None:
        self._user = user
        self._config = config
        self._current_flow: Optional[Dict] = None
        self._default_flow = next(
            (f for f in config.flows if f['messages'] is None), None)

    def _check_messages(self, flow: Dict, text: str) -> bool:
        messages = flow['messages']
        if messages is None:
            return False
        for msg in messages:
            if re.match(msg, text, flags=re.I) is not None:
                return True
        return False

    def message(self, text: str) -> Optional[str]:
        if self._current_flow is not None:
            flow = self._current_flow
            self._current_flow = None
        else:
            flow = self._default_flow
            for f in self._config.flows:
                if self._check_messages(f, text):
                    flow = f
                    break
        if flow is None:
            return None
        self._current_flow = flow['response'].get('flow')
        plugins = Plugins(text)
        return flow['response']['msg'].format(
            user=asdict(self._user), plugins=plugins, message=text)
