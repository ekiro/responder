from dataclasses import dataclass
from typing import List


@dataclass
class User:
    name: str


@dataclass
class Config:
    flows: List
