from typing import TextIO

import yaml

from responder.conversation.model import Config


class ConfigLoader:
    def load(self, reader: TextIO) -> Config:
        data = yaml.load_all(reader, Loader=yaml.Loader)
        return Config(flows=list(data))
