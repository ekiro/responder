import os
from datetime import datetime

import requests

OW_URL = "https://api.openweathermap.org/data/2.5/weather?q={city}" \
         "&appid={key}&units=metric"

class Plugins:
    def __init__(self, text: str) -> None:
        self._text = text
        self._open_weather_api_key = os.getenv("WEATHER_API_KEY")

    @property
    def time(self) -> str:
        return datetime.now().strftime("%H:%M")

    @property
    def weather(self) -> str:
        url = OW_URL.format(city=self._text, key=self._open_weather_api_key)
        res = requests.get(url)
        if res.status_code != 200:
            if res.status_code == 404:
                return "Nie znalazłem takiego miasta"
            return f"Błąd: {res.text}"
        data = res.json()
        return f"""Pogoda w {data['name']}:
    Temperatura: {data['main']['temp']}°C
    Odczuwalna: {data['main']['feels_like']}°C
    Prędkość wiatru {data['wind']['speed']} km/h
        """
