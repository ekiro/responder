import sys

from responder.conversation.config import ConfigLoader
from responder.conversation.conversation import Conversation
from responder.conversation.model import User
from responder.web.server import WebServer

if __name__ == '__main__':
    with open("data/conversation.yaml", encoding="utf-8") as f:
        cfg = ConfigLoader().load(f)

    if len(sys.argv) > 1 and sys.argv[1] == 'server':
        WebServer(cfg).run()
    else:
        user = User(name=input("Podaj imię: "))
        conv = Conversation(user, cfg)
        while True:
            response = conv.message(input("> "))
            if response:
                print(response)
